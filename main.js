import Vue from 'vue'
import App from './App'

import operate from "common/operate.js" //全局js
Vue.prototype.$operate = operate

import store from "store/index.js"

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
