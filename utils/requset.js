import operate from '@/common/operate.js'
import store from '@/store/index.js'

export default class Request {
	http(param) {
		// 请求参数
		var url = param.url,
			method = param.method,
			header = {},
			data = param.data || {},
			token = param.token || "",
			hideLoading = param.hideLoading || false;

		//拼接完整请求地址
		var requestUrl = operate.api + url;

		//固定参数:仅仅在小程序绑定页面通过code获取token的接口默认传递了参数token = login
		// if (!data.token) { //其他业务接口传递过来的参数中无token
		// 	// token = uni.getStorageSync(this.sessionKey); //参数中无token时在本地缓存中获取
		// 	token = store.userInfo.token; //参数中无token时在本地缓存中获取
		// 	console.log("当前token:" + token);
		// 	if (!token) { //本地无token需重新登录(退出时清缓存token)
		// 		_self.login(backpage, backtype);
		// 		return;
		// 	} else {
		// 		data.token = token;
		// 	}
		// }

		//时间戳
		// var timestamp = Date.parse(new Date());
		// data["timestamp"] = timestamp;
		// // #ifdef MP-WEIXIN
		// data["device"] = "wxapp";
		// data["ver"] = "1.1.30";
		// // #endif
		// // #ifdef APP-PLUS || H5
		// data["device"] = "iosapp";
		// data["ver"] = "1.0.0";
		// // #endif


		//请求方式:GET或POST(POST需配置
		// header: {'content-type' : "application/x-www-form-urlencoded"},)
		if (method) {
			method = method.toUpperCase(); //小写改为大写
			if (method == "POST") {
				header = {
					'content-type': "application/x-www-form-urlencoded"
				};
			} else {
				header = {
					'content-type': "application/json"
				};
			}
		} else {
			method = "GET";
			header = {
				'content-type': "application/json"
			};
		}
		//用户交互:加载圈
		if (!hideLoading) {
			uni.showLoading({
				title: '加载中...'
			});
		}

		// 返回promise
		return new Promise((resolve, reject) => {
			// 请求
			uni.request({
				url: requestUrl,
				data: data,
				method: method,
				header: header,
				success: (res) => {
					// 判断 请求api 格式是否正确
					if (res.statusCode && res.statusCode != 200) {
						uni.showToast({
							title: "api错误" + res.errMsg,
							icon: 'none'
						});
						return;
					}
					// code判断:200成功,201错误,-1未登录(未绑定/失效/被解绑)
					if (res.data.code) {
						if (res.data.code == '-1') {
							//没有登陆跳转登陆用
							uni.navigateTo({
								url: '/pages/login/login.vue'
							})
							return;
						}
						if (res.data.code != '200') {
							uni.showToast({
								title: "" + res.data.msg,
								icon: 'none'
							});
							return;
						}
					} else {
						uni.showToast({
							title: "code!=200" + res.data.msg,
							icon: 'none'
						});
						return;
					}
					// 将结果抛出
					resolve(res)
				},
				//请求失败
				fail: (e) => {
					uni.showToast({
						title: "" + res.data.msg,
						icon: 'none'
					});
					resolve(e);
				},
				//请求完成
				complete() {
					//隐藏加载
					if (!hideLoading) {
						uni.hideLoading();
					}
					resolve();
					return;
				}
			})
		})
	}
}
