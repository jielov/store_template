import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		//用户是否登录
		hasLogin: false,
		//用户数据
		userInfo: {}
	},
	mutations: {
		login(state, provider) {
			state.hasLogin = true;
			state.userInfo = provider;
			uni.setStorage({
				key: 'userInfo',
				data: provider
			});
			console.log('用户登录数据', state.userInfo);
		},
		logout(state) {
			state.hasLogin = false,
				state.userInfo = {},
				uni.removeSavedFile({
					key: 'userInfo'
				})
		}
	}
})

export default store
