export default {
	//接口
	api: "http://localhost:3000/",

	//跳转页面
	redirect: function(url) {
		uni.navigateTo({
			url: url,
			animationType: 'pop-in',
			animationDuration: 300
		})
	},

	//吐司
	toast: function(title) {
		uni.showToast({
			title: title,
			icon: 'none'
		});
	},

	//隐藏手机号中间位
	hidePhone: function(number) {
		let newStr = '';
		newStr = number.slice(0, 3) + '****' + number.slice(7);
		return newStr;
	},

	//去空格
	trim: function(value) {
		return value.replace(/(^\s*)|(\s*$)/g, "");
	},

	//内容替换
	replaceAll: function(text, repstr, newstr) {
		return text.replace(new RegExp(repstr, "gm"), newstr);
	},

}
