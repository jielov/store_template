import Request from '@/utils/requset.js'
let request = new Request().http

//全局定义请求头
export default {
	sendRequest: function(data) {
		return request({
			url: "banner", //请求头
			method: "GET", //请求方式
			data: data, //请求数据
			hideLoading: false, //加载样式
		})
	},
}
/*
全局请求样式：
	自定义名字: function(data) {
		return request({
			url: "/banner", //请求头
			method: "GET", //请求方式
			data: data,	//请求数据
			token: token, // 可传  不传从本地缓存获取
			hideLoading: false, //加载样式
		})
	},
页面请求方法样式
	1.先导入本页面
		import api from '@/common/api.js'
		
	2.在methods 中 调用：
		api.sendRequest().then((res) => {
			console.log(res);
		})
*/
